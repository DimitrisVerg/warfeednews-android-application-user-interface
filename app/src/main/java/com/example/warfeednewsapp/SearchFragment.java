package com.example.warfeednewsapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static com.example.warfeednewsapp.R.drawable;
import static com.example.warfeednewsapp.R.id;
import static com.example.warfeednewsapp.R.layout;

public class SearchFragment extends Fragment {

    List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
    ListView listViewOnItemClick;

    String[] countriesName = {"Afghanistan", "Albania", "Armenia", "Austria", "Azerbaijan", "Bulgaria", "Croatia", "Cyprus", "Czech Republic", "Egypt", "Estonia", "Finland", "France", "Georgia", "Germany"
            , "Greece", "Hungary", "Iceland", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jordan", "Kazahstan", "Kosovo", "Kurdistan", "Latvia", "Lebanon", "Liberia", "Lithuania", "Luxembourg", "Malaysia",
            "Malta", "Moldavia", "Monaco", "Montenegro", "Morocco", "Netherlands", "New Zealand", "North Macedonia", "Norway", "Palestine", "Poland", "Portugal", "Quatar", "Romania", "Russia", "San Marino",
            "Saudi Arabia", "Scotland", "Serbia", "Slovenia", "Spain", "Sweden", "Syria", "Turkey", "Ukraine", "United Kingdom (U.K)", "United States of America (U.S.A)", "Vietnam", "Yemen"};
    int[] countriesImage = {drawable.flag_of_afghanistan, drawable.flag_of_albania, drawable.flag_of_armenia, drawable.flag_of_austria, drawable.flag_of_azerbaijan, drawable.flag_of_bulgaria, drawable.flag_of_croatia
            , drawable.flag_of_cyprus, drawable.flag_of_czech_republic, drawable.flag_of_egypt, drawable.flag_of_estonia, drawable.flag_of_finland, drawable.flag_of_france, drawable.flag_of_georgia, drawable.flag_of_germany
            , drawable.flag_of_greece, drawable.flag_of_hungary, drawable.flag_of_iceland, drawable.flag_of_iran, drawable.flag_of_iraq, drawable.flag_of_ireland, drawable.flag_of_israel, drawable.flag_of_italy, drawable.flag_of_jordan
            , drawable.flag_of_kazahstan, drawable.flag_of_kosovo, drawable.flag_of_kurdistan, drawable.flag_of_latvia, drawable.flag_of_lebanon, drawable.flag_of_liberia, drawable.flag_of_lithuania, drawable.flag_of_luxembourg
            , drawable.flag_of_malaysia, drawable.flag_of_malta, drawable.flag_of_moldavia, drawable.flag_of_monaco, drawable.flag_of_montenegro, drawable.flag_of_morocco, drawable.flag_of_netherlands, drawable.flag_of_new_zealand
            , drawable.flag_of_north_macedonia, drawable.flag_of_norway, drawable.flag_of_palestine, drawable.flag_of_poland, drawable.flag_of_portugal, drawable.flag_of_quatar, drawable.flag_of_romania, drawable.flag_of_russia
            , drawable.flag_of_san_marino, drawable.flag_of_saudi_arabia, drawable.flag_of_scotland, drawable.flag_of_serbia, drawable.flag_of_slovenia, drawable.flag_of_spain, drawable.flag_of_sweden, drawable.flag_of_syria
            , drawable.flag_of_turkey, drawable.flag_of_ukraine, drawable.flag_of_united_kingdom, drawable.flag_of_united_states, drawable.flag_of_vietnam, drawable.flag_of_yemen};

    public SearchFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layout.fragment_search,container,false);
        final ListView listView = (ListView) view.findViewById(id.listView);
        SearchView searchView = (SearchView) view.findViewById(R.id.searchView);
        listViewOnItemClick = listView;

        for (int i = 0; i < countriesName.length; i++) {
            HashMap<String,String> map = new HashMap<String, String>();
            map = new HashMap<String, String>();
            map.put("CountriesName", countriesName[i]);
            map.put("CountriesImage", Integer.toString(countriesImage[i]));

            list.add(map);
        }

        String[] from = {"CountriesName", "CountriesImage"};
        int[] to = {id.tvCountry, id.imageCountry};
        final SimpleAdapter adapter = new SimpleAdapter(getActivity(), list, layout.row_data, from, to);
        listView.setAdapter(adapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String text) {
                if(list.contains(text)){
                    adapter.getFilter().filter(text);
                }else{
                    Toast.makeText(getActivity(), "No Match found",Toast.LENGTH_LONG).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        listViewOnItemClick.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), list.get(position).get("CountriesName"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), CountryData.class);
                String text = countriesName[position].toString();
                intent.putExtra("TextValue",text);
                startActivity(intent);
            }
        });
    }
}