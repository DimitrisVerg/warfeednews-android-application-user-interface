package com.example.warfeednewsapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


public class CountryData extends AppCompatActivity  {

    private static final String key_coordinates = "coordinates";
    private static final String key_timestamp = "time_stamp";
    ToggleButton toggleButton;
    TextView text;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_countries_data);

            final TextView textCoordinates = (TextView) findViewById(R.id.Coordinates);
            final TextView textTime = (TextView) findViewById(R.id.Time);

            text = (TextView)findViewById(R.id.Country);
            String holder = getIntent().getStringExtra("TextValue");
            text.setText(holder);

            toggleButton = (ToggleButton) findViewById(R.id.favbtn);
            toggleButton.setChecked(false);
            toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black_50dp));
            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isChecked = false;
                    Snackbar.make(toggleButton, "Added to Favorite", Snackbar.LENGTH_SHORT).show();
                    toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_black_50dp));
                    saveState(isChecked);
                }else{
                    Snackbar.make(toggleButton, "Removed from Favorite", Snackbar.LENGTH_SHORT).show();
                    isChecked = true;
                    toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black_50dp));
                    saveState(isChecked);
                }
            }
        });


            //Before run change the ip address to your pc's id here and from the file network_security_config.xml
            String url = "http://192.168.2.7:8088/tweets/"+holder.toLowerCase();
            final RequestQueue queue = Volley.newRequestQueue(this);
            // final Map<String,String> ok = new HashMap<>();
            JsonArrayRequest objectRequest = new JsonArrayRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("REST response",response.toString());

                            try {
                                for (int i=0;i<response.length();i++){
                                    JSONObject res = response.getJSONObject(i);
                                    String timestamp = res.getString(key_timestamp);
                                    String coordinates = res.getString(key_coordinates);
                                    textCoordinates.setText(coordinates);
                                    textTime.setText(timestamp);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Rest response",error.toString());
                        }
                    }
            );
            queue.add(objectRequest);
            
    }

    private void saveState(boolean isFavourite) {
        SharedPreferences aSharedPreferences = this.getSharedPreferences("Favourite", Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences.edit();
        aSharedPreferencesEdit.putBoolean("State", isFavourite);
        aSharedPreferencesEdit.commit();
    }

}
