package com.example.warfeednewsapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.FragmentManager;

public class HomeFragment extends Fragment {

    DrawerLayout drawer;
    NavigationView navigationView;
    View headerLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_home,container,false);

        drawer = (DrawerLayout) view.findViewById(R.id.slidemenu);
        navigationView = (NavigationView)view.findViewById(R.id.nvView);
        headerLayout = navigationView.inflateHeaderView(R.layout.headerslide);
        AppCompatActivity appCompatActivity = (AppCompatActivity)getActivity();
      //  appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //  appCompatActivity.getSupportActionBar().setHomeButtonEnabled(true);
        setupDrawerContent(navigationView);
        return view;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.aboutus:
                fragmentClass = AboutUs.class;
                break;
            case R.id.favorites:
                fragmentClass = Favorites.class;
                break;
            default:
                fragmentClass = HomeFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fraigment_container, fragment).commit();
        drawer.closeDrawers();
    }

}