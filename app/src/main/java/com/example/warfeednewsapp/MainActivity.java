package com.example.warfeednewsapp;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapbox.mapboxsdk.Mapbox;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Mapbox.getInstance(this, "pk.eyJ1Ijoic2FraXNtYXoiLCJhIjoiY2szdnI0cHd4MDkyYzNqcDdla2NzbmN2ZyJ9.c0c34W_pzX-K-lcto0kZIQ");

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        //getSupportFragmentManager().beginTransaction().replace(R.id.fraigment_container,new HomeFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.na_home:
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.na_Statistics:
                            selectedFragment = new StatisticsFragment();
                            break;
                        case R.id.na_search:
                            selectedFragment = new SearchFragment();
                            break;
                        case R.id.na_map:
                            selectedFragment = new MapFragment1();
                    }
                        getSupportFragmentManager().beginTransaction().replace(R.id.fraigment_container,selectedFragment).commit();
                    return true;
                }
            };
}
