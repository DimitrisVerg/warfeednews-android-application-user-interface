package com.example.warfeednewsapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;

public class StatisticsFragment extends Fragment {
    private static final String key_country = "country";
    private static final String key_number = "tid";
    final static Map<String, String> countries = new ArrayMap<>();
    final  static List<String> geJcountry = new ArrayList<>();
    final static List<String> geJnum = new ArrayList<>();
    final static List<DataEntry> dataEntries = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        AnyChartView anyChartView = (AnyChartView) view.findViewById(R.id.any_chart_view);

        setupPieChart(anyChartView,countries);
        return view;
    }

    public void setupPieChart(AnyChartView anyChartView, Map<String, String> countries) {
        dataEntries.clear();
        Pie pie = AnyChart.pie();

        for (int i=0; i<countries.size(); i++){
            dataEntries.add(new ValueDataEntry(geJcountry.get(i),Integer.parseInt(geJnum.get(i))));
        }
        pie.data(dataEntries);
        anyChartView.setChart(pie);
    }

    @Override
    public void onStart(){
        final Map<String, String> contains = new HashMap<>();
        super.onStart();
        String url = "http://192.168.2.7:8088/tweets";
        final RequestQueue queue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
        JsonArrayRequest objectRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("Rest Response",response.toString());
                        try {
                            for (int i=0;i<response.length();i++){
                                JSONObject res = response.getJSONObject(i);
                                geJcountry.add(res.getString(key_country));
                                geJnum.add(res.getString(key_number));
                                //Todo:Create a method for get number of countries!!
                                countries.put(geJcountry.get(i),geJnum.get(i));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest error Response",error.toString());
                    }
                }
        );
        queue.add(objectRequest);
    }
}

