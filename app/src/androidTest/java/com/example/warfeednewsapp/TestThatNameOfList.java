package com.example.warfeednewsapp;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class TestThatNameOfList {

    @Rule
    public ActivityTestRule<SplashScreen> mActivityTestRule = new ActivityTestRule<>(SplashScreen.class);

    @Test
    public void testThatNameOfList() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html

        try {
            Thread.sleep(3100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction bottomNavigationItemView = onView(
                allOf(withId(R.id.na_search), withContentDescription("Search"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottom_navigation),
                                        0),
                                2),
                        isDisplayed()));
        bottomNavigationItemView.perform(click());

        DataInteraction relativeLayout = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(0);
        relativeLayout.perform(click());

        ViewInteraction textView = onView(withId(R.id.Country));
        textView.check(matches(withText("Afghanistan")));

        pressBack();

        DataInteraction relativeLayout2 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(1);
        relativeLayout2.perform(click());

        ViewInteraction textView2 = onView(withId(R.id.Country));
        textView2.check(matches(withText("Albania")));

        pressBack();

        DataInteraction relativeLayout3 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(2);
        relativeLayout3.perform(click());

        ViewInteraction textView3 = onView(withId(R.id.Country));
        textView3.check(matches(withText("Armenia")));

        pressBack();

        DataInteraction relativeLayout4 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(3);
        relativeLayout4.perform(click());

        ViewInteraction textView4 = onView(withId(R.id.Country));
        textView4.check(matches(withText("Austria")));

        pressBack();

        DataInteraction relativeLayout5 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(4);
        relativeLayout5.perform(click());

        ViewInteraction textView5 = onView(withId(R.id.Country));
        textView5.check(matches(withText("Azerbaijan")));

        pressBack();

        DataInteraction relativeLayout6 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(5);
        relativeLayout6.perform(click());

        ViewInteraction textView6 = onView(withId(R.id.Country));
        textView6.check(matches(withText("Bulgaria")));

        pressBack();

        DataInteraction relativeLayout7 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(6);
        relativeLayout7.perform(click());

        ViewInteraction textView7 = onView(withId(R.id.Country));
        textView7.check(matches(withText("Croatia")));

        pressBack();

        DataInteraction relativeLayout8 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(7);
        relativeLayout8.perform(click());

        ViewInteraction textView8 = onView(withId(R.id.Country));
        textView8.check(matches(withText("Cyprus")));

        pressBack();

        DataInteraction relativeLayout9 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(8);
        relativeLayout9.perform(click());

        ViewInteraction textView9 = onView(withId(R.id.Country));
        textView9.check(matches(withText("Czech Republic")));

        pressBack();

        DataInteraction relativeLayout10 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(9);
        relativeLayout10.perform(click());

        ViewInteraction textView10 = onView(withId(R.id.Country));
        textView10.check(matches(withText("Egypt")));

        pressBack();

        DataInteraction relativeLayout11 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(10);
        relativeLayout11.perform(click());

        ViewInteraction textView11 = onView(withId(R.id.Country));
        textView11.check(matches(withText("Estonia")));

        pressBack();

        DataInteraction relativeLayout12 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(11);
        relativeLayout12.perform(click());

        ViewInteraction textView12 = onView(withId(R.id.Country));
        textView12.check(matches(withText("Finland")));

        pressBack();

        DataInteraction relativeLayout13 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(12);
        relativeLayout13.perform(click());

        ViewInteraction textView13 = onView(withId(R.id.Country));
        textView13.check(matches(withText("France")));

        pressBack();

        DataInteraction relativeLayout14 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(13);
        relativeLayout14.perform(click());

        ViewInteraction textView14 = onView(withId(R.id.Country));
        textView14.check(matches(withText("Georgia")));

        pressBack();

        DataInteraction relativeLayout15 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(14);
        relativeLayout15.perform(click());

        ViewInteraction textView15 = onView(withId(R.id.Country));
        textView15.check(matches(withText("Germany")));

        pressBack();

        DataInteraction relativeLayout16 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(15);
        relativeLayout16.perform(click());

        ViewInteraction textView16 = onView(withId(R.id.Country));
        textView16.check(matches(withText("Greece")));

        pressBack();

        DataInteraction relativeLayout17 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(16);
        relativeLayout17.perform(click());

        ViewInteraction textView17 = onView(withId(R.id.Country));
        textView17.check(matches(withText("Hungary")));
        pressBack();

        DataInteraction relativeLayout18 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(17);
        relativeLayout18.perform(click());

        ViewInteraction textView18 = onView(withId(R.id.Country));
        textView18.check(matches(withText("Iceland")));
        pressBack();

        DataInteraction relativeLayout19 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(18);
        relativeLayout19.perform(click());

        ViewInteraction textView19 = onView(withId(R.id.Country));
        textView19.check(matches(withText("Iran")));
        pressBack();

        DataInteraction relativeLayout20 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(19);
        relativeLayout20.perform(click());

        ViewInteraction textView20 = onView(withId(R.id.Country));
        textView20.check(matches(withText("Iraq")));
        pressBack();

        DataInteraction relativeLayout21 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(20);
        relativeLayout21.perform(click());

        ViewInteraction textView21 = onView(withId(R.id.Country));
        textView21.check(matches(withText("Ireland")));
        pressBack();

        DataInteraction relativeLayout22 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(21);
        relativeLayout22.perform(click());

        ViewInteraction textView22 = onView(withId(R.id.Country));
        textView22.check(matches(withText("Israel")));
        pressBack();

        DataInteraction relativeLayout23 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(22);
        relativeLayout23.perform(click());

        ViewInteraction textView23 = onView(withId(R.id.Country));
        textView23.check(matches(withText("Italy")));
        pressBack();

        DataInteraction relativeLayout24 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(23);
        relativeLayout24.perform(click());

        ViewInteraction textView24 = onView(withId(R.id.Country));
        textView24.check(matches(withText("Jordan")));
        pressBack();

        DataInteraction relativeLayout25 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(24);
        relativeLayout25.perform(click());

        ViewInteraction textView25 = onView(withId(R.id.Country));
        textView25.check(matches(withText("Kazahstan")));
        pressBack();

        DataInteraction relativeLayout26 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(25);
        relativeLayout26.perform(click());

        ViewInteraction textView26 = onView(withId(R.id.Country));
        textView26.check(matches(withText("Kosovo")));
        pressBack();

        DataInteraction relativeLayout27 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(26);
        relativeLayout27.perform(click());

        ViewInteraction textView27 = onView(withId(R.id.Country));
        textView27.check(matches(withText("Kurdistan"))); pressBack();

        DataInteraction relativeLayout28 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(27);
        relativeLayout28.perform(click());

        ViewInteraction textView28 = onView(withId(R.id.Country));
        textView28.check(matches(withText("Latvia")));
        pressBack();

        DataInteraction relativeLayout29 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(28);
        relativeLayout29.perform(click());

        ViewInteraction textView29 = onView(withId(R.id.Country));
        textView29.check(matches(withText("Lebanon")));
        pressBack();

        DataInteraction relativeLayout30 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(29);
        relativeLayout30.perform(click());

        ViewInteraction textView30 = onView(withId(R.id.Country));
        textView30.check(matches(withText("Liberia"))); pressBack();

        DataInteraction relativeLayout31 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(30);
        relativeLayout31.perform(click());

        ViewInteraction textView31 = onView(withId(R.id.Country));
        textView31.check(matches(withText("Lithuania")));
        pressBack();

        DataInteraction relativeLayout32 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(31);
        relativeLayout32.perform(click());

        ViewInteraction textView32 = onView(withId(R.id.Country));
        textView32.check(matches(withText("Luxembourg")));
        pressBack();

        DataInteraction relativeLayout33 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(32);
        relativeLayout33.perform(click());

        ViewInteraction textView33 = onView(withId(R.id.Country));
        textView33.check(matches(withText("Malaysia")));
        pressBack();

        DataInteraction relativeLayout34 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(33);
        relativeLayout34.perform(click());

        ViewInteraction textView34 = onView(withId(R.id.Country));
        textView34.check(matches(withText("Malta")));
        pressBack();

        DataInteraction relativeLayout35 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(34);
        relativeLayout35.perform(click());

        ViewInteraction textView35 = onView(withId(R.id.Country));
        textView35.check(matches(withText("Moldavia")));
        pressBack();

        DataInteraction relativeLayout36 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(35);
        relativeLayout36.perform(click());

        ViewInteraction textView36 = onView(withId(R.id.Country));
        textView36.check(matches(withText("Monaco")));
        pressBack();

        DataInteraction relativeLayout37 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(36);
        relativeLayout37.perform(click());

        ViewInteraction textView37 = onView(withId(R.id.Country));
        textView37.check(matches(withText("Montenegro")));
        pressBack();

        DataInteraction relativeLayout38 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(37);
        relativeLayout38.perform(click());

        ViewInteraction textView38 = onView(withId(R.id.Country));
        textView38.check(matches(withText("Morocco")));
        pressBack();

        DataInteraction relativeLayout39 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(38);
        relativeLayout39.perform(click());

        ViewInteraction textView39 = onView(withId(R.id.Country));
        textView39.check(matches(withText("Netherlands"))); pressBack();

        DataInteraction relativeLayout40 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(39);
        relativeLayout40.perform(click());

        ViewInteraction textView40 = onView(withId(R.id.Country));
        textView40.check(matches(withText("New Zealand"))); pressBack();

        DataInteraction relativeLayout41 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(40);
        relativeLayout41.perform(click());

        ViewInteraction textView41 = onView(withId(R.id.Country));
        textView41.check(matches(withText("North Macedonia")));
        pressBack();

        DataInteraction relativeLayout42 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(41);
        relativeLayout42.perform(click());

        ViewInteraction textView42 = onView(withId(R.id.Country));
        textView42.check(matches(withText("Norway")));
        pressBack();

        DataInteraction relativeLayout43 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(42);
        relativeLayout43.perform(click());

        ViewInteraction textView43 = onView(withId(R.id.Country));
        textView43.check(matches(withText("Palestine")));
        pressBack();

        DataInteraction relativeLayout44 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(43);
        relativeLayout44.perform(click());

        ViewInteraction textView44 = onView(withId(R.id.Country));
        textView44.check(matches(withText("Poland")));
        pressBack();

        DataInteraction relativeLayout45 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(44);
        relativeLayout45.perform(click());

        ViewInteraction textView45 = onView(withId(R.id.Country));
        textView45.check(matches(withText("Portugal")));
        pressBack();

        DataInteraction relativeLayout46 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(45);
        relativeLayout46.perform(click());

        ViewInteraction textView46 = onView(withId(R.id.Country));
        textView46.check(matches(withText("Quatar")));
        pressBack();

        DataInteraction relativeLayout47 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(46);
        relativeLayout47.perform(click());

        ViewInteraction textView47 = onView(withId(R.id.Country));
        textView47.check(matches(withText("Romania")));
        pressBack();

        DataInteraction relativeLayout48 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(47);
        relativeLayout48.perform(click());

        ViewInteraction textView48 = onView(withId(R.id.Country));
        textView48.check(matches(withText("Russia")));
        pressBack();

        DataInteraction relativeLayout49 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(48);
        relativeLayout49.perform(click());

        ViewInteraction textView49 = onView(withId(R.id.Country));
        textView49.check(matches(withText("San Marino")));
        pressBack();

        DataInteraction relativeLayout50 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(49);
        relativeLayout50.perform(click());

        ViewInteraction textView50 = onView(withId(R.id.Country));
        textView50.check(matches(withText("Saudi Arabia")));
        pressBack();

        DataInteraction relativeLayout51 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(50);
        relativeLayout51.perform(click());

        ViewInteraction textView51 = onView(withId(R.id.Country));
        textView51.check(matches(withText("Scotland")));
        pressBack();

        DataInteraction relativeLayout52 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(51);
        relativeLayout52.perform(click());

        ViewInteraction textView52 = onView(withId(R.id.Country));
        textView52.check(matches(withText("Serbia")));
        pressBack();

        DataInteraction relativeLayout53 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(52);
        relativeLayout53.perform(click());

        ViewInteraction textView53 = onView(withId(R.id.Country));
        textView53.check(matches(withText("Slovenia")));
        pressBack();

        DataInteraction relativeLayout54 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(53);
        relativeLayout54.perform(click());

        ViewInteraction textView54 = onView(withId(R.id.Country));
        textView54.check(matches(withText("Spain")));
        pressBack();

        DataInteraction relativeLayout55 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(54);
        relativeLayout55.perform(click());

        ViewInteraction textView55 = onView(withId(R.id.Country));
        textView55.check(matches(withText("Sweden")));
        pressBack();

        DataInteraction relativeLayout56 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(55);
        relativeLayout56.perform(click());

        ViewInteraction textView56 = onView(withId(R.id.Country));
        textView56.check(matches(withText("Syria")));
        pressBack();

        DataInteraction relativeLayout57 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(56);
        relativeLayout57.perform(click());

        ViewInteraction textView58 = onView(withId(R.id.Country));
        textView58.check(matches(withText("Turkey")));
        pressBack();

        DataInteraction relativeLayout59 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(57);
        relativeLayout59.perform(click());

        ViewInteraction textView59 = onView(withId(R.id.Country));
        textView59.check(matches(withText("Ukraine")));
        pressBack();

        DataInteraction relativeLayout60 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(58);
        relativeLayout60.perform(click());

        ViewInteraction textView60 = onView(withId(R.id.Country));
        textView60.check(matches(withText("United Kingdom (U.K)")));
        pressBack();

        DataInteraction relativeLayout61 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(59);
        relativeLayout61.perform(click());

        ViewInteraction textView61 = onView(withId(R.id.Country));
        textView61.check(matches(withText("United States of America (U.S.A)")));
        pressBack();

        DataInteraction relativeLayout62 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(60);
        relativeLayout62.perform(click());

        ViewInteraction textView62 = onView(withId(R.id.Country));
        textView62.check(matches(withText("Vietnam")));
        pressBack();

        DataInteraction relativeLayout63 = onData(anything())
                .inAdapterView(allOf(withId(R.id.listView),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                0)))
                .atPosition(61);
        relativeLayout63.perform(click());

        ViewInteraction textView63 = onView(withId(R.id.Country));
        textView63.check(matches(withText("Yemen")));
        pressBack();
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
